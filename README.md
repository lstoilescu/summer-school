# summer-school

Session 1 for Thales Summer School
Engineering Tools: Git/ BitBucket & Nexus

*Prerequisites:* 
1. Get Git
2. Create SSH credentials 
    `ssh-keygen -t rsa -C <emailaddress>`
    `cat <path_pub> | clip`
    Copy clip to GitLab User settings SSH keys

*Steps:*
1. Clone repository https://gitlab.com/lstoilescu/summer-school.git `git clone https://gitlab.com/lstoilescu/summer-school.git `
    1. Move to `summer-school` directory  `cd summer-school`
    2. Move to Development branch `git checkout Development`
2. Create local branch  `git branch student#`
3. Checkout new branch  `git checkout student#`
4. Edit students.txt with information for each student `git status`
5. Commit change   `git add students.txt`, `git commit -m "Added details for student#"`
4. Push to student# branch `git push --set-upstream origin student#`
5. Switch to Development branch `git checkout Development`
6. Merge changes from individual branch to Development `git merge student#`
7. Checkout your own branch `git checkout student#`
8. Make sure you are up to date  `git pull` (pull = fetch+ merge)
    Check out what else happened with `git branch`
9. Edit again the students.txt and add a line to your profile with "Observations" `git status`
10. Commit new change `git commit -m "Added observations for student#"`

11. Push to upstream branch `git push`
12. Checkout Development branch `git checkout Development`
13. Merge new changes `git merge student#`
7. Delete individual branch `git branch`, `git branch -d student#`
8. Did you git it? 
